from django.conf.urls import  url
from django.utils.translation import ugettext as _
from . import views
from django.contrib.auth import views as vi

from authentication.forms import SetPasswordFormEdited, AuthenticationFormEdited


urlpatterns = [
                        url(r'^login/$', vi.login, {
                            'template_name': 'authentication/login.html',
                            'authentication_form': AuthenticationFormEdited,
                            'extra_context': {
                                'page': {
                                    'title': _('Log in'),
                                },
                            }
                        }, name='login'),
                        url(r'^logout/$', vi.logout, {
                            'next_page': '/',
                        }, name='logout'),
                       

                        ]

