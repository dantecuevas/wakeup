# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-01-08 21:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panel', '0009_auto_20200108_1523'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='facebook',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Facebook'),
        ),
        migrations.AlterField(
            model_name='team',
            name='instagram',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Instagram'),
        ),
        migrations.AlterField(
            model_name='team',
            name='mail',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Mail'),
        ),
        migrations.AlterField(
            model_name='team',
            name='testimony',
            field=models.TextField(blank=True, null=True, verbose_name='Testimonio'),
        ),
        migrations.AlterField(
            model_name='team',
            name='twitter',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Twitter'),
        ),
    ]
